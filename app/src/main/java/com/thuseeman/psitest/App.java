package com.thuseeman.psitest;

import android.app.Application;

import com.thuseeman.psitest.di.AppComponent;
import com.thuseeman.psitest.di.AppModule;
import com.thuseeman.psitest.di.DaggerAppComponent;

import timber.log.Timber;

/**
 * Created by thuseeman on 4/12/18.
 */

public class App extends Application{

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this)).build();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
