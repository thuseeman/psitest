package com.thuseeman.psitest.data.remote.response;

import com.google.gson.annotations.SerializedName;
import com.thuseeman.psitest.ui.PsiViewModel;

import java.util.List;

/**
 * Created by thuseeman on 4/11/18.
 */

public class PsiResponse {
    @SerializedName("region_metadata")
    private List<RegionalInfo> regionalInfo;

    private List<Item> items;

    @SerializedName("api_info")
    private ApiInfo apiInfo;

    public List<RegionalInfo> getRegionalInfo() {
        return regionalInfo;
    }

    public List<Item> getItems() {
        return items;
    }

    public ApiInfo getApiInfo() {
        return apiInfo;
    }

    public static class LocationInfo {
        private double latitude;
        private double longitude;

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }
    }

    public static class RegionalInfo {
        private String name;
        @SerializedName("label_location")
        private LocationInfo locationInfo;

        public String getName() {
            return name;
        }

        public LocationInfo getLocationInfo() {
            return locationInfo;
        }
    }

    public static class Item {
        @SerializedName("timestamp")
        private String timeStamp;

        @SerializedName("update_timestamp")
        private String updateTimeStamp;

        private Readings readings;

        public String getTimeStamp() {
            return timeStamp;
        }

        public String getUpdateTimeStamp() {
            return updateTimeStamp;
        }

        public Readings getReadings() {
            return readings;
        }
    }

    public static class Readings {
        @SerializedName("co_eight_hour_max")
        private Reading coEightHourMax;

        @SerializedName("co_sub_index")
        private Reading coSubNndex;

        @SerializedName("no2_one_hour_max")
        private Reading no2OneHourMax;

        @SerializedName("o3_eight_hour_max")
        private Reading o3EighHourMax;

        @SerializedName("o3_sub_index")
        private Reading o3SubIndex;

        @SerializedName("pm10_sub_index")
        private Reading pm10SubIndex;

        @SerializedName("pm10_twenty_four_hourly")
        private Reading pm10TwentyFourJourly;

        @SerializedName("pm25_sub_index")
        private Reading pm25SubIndex;

        @SerializedName("pm25_twenty_four_hourly")
        private Reading pm25TwentyFourHourly;

        @SerializedName("psi_twenty_four_hourly")
        private Reading psiTwentyFourHourly;

        @SerializedName("so2_sub_index")
        private Reading so2SubIndex;

        @SerializedName("so2_twenty_four_hourly")
        private Reading so2TwentyFourHourly;

        public Reading getCoEightHourMax() {
            return coEightHourMax;
        }

        public Reading getCoSubNndex() {
            return coSubNndex;
        }

        public Reading getNo2OneHourMax() {
            return no2OneHourMax;
        }

        public Reading getO3EighHourMax() {
            return o3EighHourMax;
        }

        public Reading getO3SubIndex() {
            return o3SubIndex;
        }

        public Reading getPm10SubIndex() {
            return pm10SubIndex;
        }

        public Reading getPm10TwentyFourJourly() {
            return pm10TwentyFourJourly;
        }

        public Reading getPm25SubIndex() {
            return pm25SubIndex;
        }

        public Reading getPm25TwentyFourHourly() {
            return pm25TwentyFourHourly;
        }

        public Reading getPsiTwentyFourHourly() {
            return psiTwentyFourHourly;
        }

        public Reading getSo2SubIndex() {
            return so2SubIndex;
        }

        public Reading getSo2TwentyFourHourly() {
            return so2TwentyFourHourly;
        }
    }

    public static class Reading {
        private double central;
        private double east;
        private double national;
        private double north;
        private double south;
        private double west;

        public double getCentral() {
            return central;
        }

        public double getEast() {
            return east;
        }

        public double getNational() {
            return national;
        }

        public double getNorth() {
            return north;
        }

        public double getSouth() {
            return south;
        }

        public double getWest() {
            return west;
        }

        public double getValue(String location) {
            switch (location) {
                case PsiViewModel.Location.CENTRAL:
                    return central;
                case PsiViewModel.Location.EAST:
                    return east;
                case PsiViewModel.Location.NORTH:
                    return north;
                case PsiViewModel.Location.SOUTH:
                    return south;
                case PsiViewModel.Location.WEST:
                    return west;
                default:
                    return national;
            }
        }
    }

    public static class ApiInfo {
        private String healthy;

        public String getHealthy() {
            return healthy;
        }
    }
}
