package com.thuseeman.psitest.data.remote;

import com.thuseeman.psitest.Config;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by thuseeman on 4/11/18.
 */

@Singleton
public class ApiRepository {

    private ApiService apiService;

    @Inject
    public ApiRepository() {
        apiService = new Retrofit.Builder()
                .baseUrl(Config.API_SG)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiService.class);
    }

    public ApiService getApiService() {
        return apiService;
    }
}
