package com.thuseeman.psitest.data.model;

import com.thuseeman.psitest.data.remote.response.PsiResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thuseeman on 4/11/18.
 */

public class LocationItem {
    private String name;
    private double latitude;
    private double longitude;
    private List<IndexData> info = new ArrayList<>();

    public LocationItem(String name, double latitude, double longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public List<IndexData> getInfo() {
        return info;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getDisplayText(){
        StringBuilder sb = new StringBuilder();
        for (IndexData s : info){
            sb.append(s.getName());
            sb.append(" : ");
            sb.append(s.getVal());
            if (info.indexOf(s) < info.size() - 1){
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    public void setPsiIndex(PsiResponse.Readings readings) {
        info.add(new IndexData("O3 Sub Index", readings.getO3SubIndex().getValue(name)));
        info.add(new IndexData("CO Sub Index", readings.getCoSubNndex().getValue(name)));
        info.add(new IndexData("SO2 Sub Index", readings.getSo2SubIndex().getValue(name)));
        info.add(new IndexData("PM10 Sub Index", readings.getPm10SubIndex().getValue(name)));
    }

    @Override
    public String toString() {
        return "LocationItem{" +
                "name='" + name + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

}
