package com.thuseeman.psitest.data.model;

/**
 * Created by thuseeman on 4/11/18.
 */

public class IndexData {
    private String name;
    private double val;

    public IndexData(String name, double val) {
        this.name = name;
        this.val = val;
    }

    public String getName() {
        return name;
    }

    public double getVal() {
        return val;
    }

}
