package com.thuseeman.psitest.data.remote;

import com.thuseeman.psitest.data.remote.response.PsiResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by thuseeman on 4/12/18.
 */

public interface ApiService {
    @GET("environment/psi")
    Call<PsiResponse> retrievePsiInfo();

}
