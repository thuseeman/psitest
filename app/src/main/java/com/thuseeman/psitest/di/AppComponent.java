package com.thuseeman.psitest.di;

import com.thuseeman.psitest.ui.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by thuseeman on 4/11/18.
 */

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(MainActivity activity);
}

