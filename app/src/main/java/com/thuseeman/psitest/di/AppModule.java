package com.thuseeman.psitest.di;

import android.content.Context;

import com.thuseeman.psitest.App;
import com.thuseeman.psitest.data.remote.ApiRepository;
import com.thuseeman.psitest.data.remote.ApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by thuseeman on 4/11/18.
 */

@Module
public class AppModule {

    private final App mApp;

    public AppModule(App mApp) {
        this.mApp = mApp;
    }

    @Singleton
    @Provides
    public Context provideContext(){
        return mApp;
    }

    @Singleton
    @Provides
    public ApiService provideApiService(ApiRepository apiRepository){
        return apiRepository.getApiService();
    }
}
