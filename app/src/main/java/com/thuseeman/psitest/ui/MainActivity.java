package com.thuseeman.psitest.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.thuseeman.psitest.R;
import com.thuseeman.psitest.data.model.LocationItem;
import com.thuseeman.psitest.data.remote.ApiService;
import com.thuseeman.psitest.ui.base.BaseActivity;

import java.util.List;

import javax.inject.Inject;

public class MainActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private GoogleMap mMap;
    private PsiViewModel mPsiViewModel;

    @Inject
    ApiService mApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
        setContentView(R.layout.activity_main);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mPsiViewModel = ViewModelProviders.of(this).get(PsiViewModel.class);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPsiViewModel.getLocations(mApiService).observe(this, locationItems -> {
            updateMap(locationItems);
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    private void updateMap(List<LocationItem> locationItemList){
        if (mMap != null){
            mMap.clear();
            LatLngBounds.Builder bounds = new LatLngBounds.Builder();
            for (LocationItem locationItem : locationItemList) {
                LatLng latLng = new LatLng(locationItem.getLatitude(), locationItem.getLongitude());
                bounds.include(latLng);
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(locationItem.getName()))
                        .setTag(locationItem);
                mMap.setOnMarkerClickListener(this);
                mMap.setInfoWindowAdapter(new LocationInfoWindowAdapter(this));
            }
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 14));
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        return false;
    }
}
