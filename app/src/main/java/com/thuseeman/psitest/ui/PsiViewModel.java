package com.thuseeman.psitest.ui;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.os.AsyncTask;

import com.thuseeman.psitest.data.model.LocationItem;
import com.thuseeman.psitest.data.remote.ApiService;
import com.thuseeman.psitest.data.remote.response.PsiResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by thuseeman on 4/12/18.
 */

public class PsiViewModel extends ViewModel {
    private MutableLiveData<List<LocationItem>> locations;
    private static final Set<String> LOCATIONS = new HashSet<>(
            Arrays.asList(Location.EAST, Location.WEST, Location.NORTH, Location.SOUTH, Location.CENTRAL));

    public LiveData<List<LocationItem>> getLocations(ApiService apiService) {
        if (locations == null) {
            locations = new MutableLiveData<>();
            retrievePsiInfo(apiService);
        }
        return locations;
    }

    private void retrievePsiInfo(ApiService apiService) {
        new AsyncTask<Void, Void, List<LocationItem>>() {

            @Override
            protected List<LocationItem> doInBackground(Void... voids) {
                List<LocationItem> locationItemList = new ArrayList<>();
                try {
                    Response<PsiResponse> response = apiService.retrievePsiInfo().execute();
                    if (response != null) {
                        if (response.isSuccessful() && response.body() != null) {
                            List<PsiResponse.Item> items = response.body().getItems();
                            List<PsiResponse.RegionalInfo> regionalInfo = response.body().getRegionalInfo();
                            for (PsiResponse.RegionalInfo info : regionalInfo) {
                                if (LOCATIONS.contains(info.getName())) {
                                    PsiResponse.LocationInfo locationInfo = info.getLocationInfo();
                                    LocationItem locationItem = new LocationItem(info.getName(), locationInfo.getLatitude(), locationInfo.getLongitude());
                                    if (items.size() > 0) {
                                        locationItem.setPsiIndex(items.get(0).getReadings());
                                    }
                                    locationItemList.add(locationItem);
                                }
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Timber.d("locationItemList:%s", Arrays.toString(locationItemList.toArray()));
                return locationItemList;
            }

            @Override
            protected void onPostExecute(List<LocationItem> locationItems) {
                locations.postValue(locationItems);
            }
        }.execute();
    }

    public interface Location{
        String EAST = "east";
        String WEST = "west";
        String NORTH = "north";
        String SOUTH = "south";
        String CENTRAL = "central";
        String NATIONAL = "national";
    }
}