package com.thuseeman.psitest.ui.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.thuseeman.psitest.App;
import com.thuseeman.psitest.di.AppComponent;

/**
 * Created by thuseeman on 4/12/18.
 */

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public AppComponent getAppComponent() {
        return ((App) getApplication()).getAppComponent();
    }

}