package com.thuseeman.psitest.ui;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.thuseeman.psitest.data.model.LocationItem;

/**
 * Created by thuseeman on 4/12/18.
 */

public class LocationInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private final Context context;

    public LocationInfoWindowAdapter(Context context) {
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        TextView textView = new TextView(context);
        LocationItem locationItem = (LocationItem) marker.getTag();
        textView.setText(locationItem.getDisplayText());
        return textView;
    }
}
